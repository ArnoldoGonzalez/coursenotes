# Inside The Engine  

Date: August 01, 2020 

Section: Section 2 JavaScript Foundation  

Principle(1 sentence after taking notes): 

Notes:

The JavaScript engine takes a JavaScript file as input. It performes lexical analysis in the Parser. Lexical analysis is the process of converting a sequence of characters into a sequence of tokens. Lexical analysis is performed by a 'lexer'. A lexer and a parser analyze teh syntax of programming languages.

Then the code that has been tokenized is formed into an AST (Abstract Syntax Tree). Once the tokenized code has been formed into an AST it is then passed to the Interpreter, then the Profiler, then the Compiler, which then outputs Optimized code that is passed to the CPU.

Simple JavaScript Engine Example

```
function jsengine(code) {
  return code.split(/\s+/) //regex
}

jsengine('var a = 5') // this outputs [ 'var', 'a', '=', '5']
```


Flash Cards:  

- Q:  
  - A:  

- Q:  
  - A:  

- Q:  
  - A:  

- Q:  
  - A:  

- Q:  
  - A:  

- Q:  
  - A:  

- Q:  
  - A:  

Sources: [Abstract Syntax Tree Explorer](https://astexplorer.net/)   


