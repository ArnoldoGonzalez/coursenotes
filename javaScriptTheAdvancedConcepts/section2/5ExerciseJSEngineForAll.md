# Exercise: JS Engine For All  

Date: August 01, 2020 

Section: Section 2 JavaScript Foundation  

Principle(1 sentence after taking notes): 

Notes: 

Exercise: What problem do you see with everybody creating their own engines in JavaScript? 

If everybody built their own engines then code would not be usable across multiple engines. It would also be a waste of time if everybody had to make their own engines. Building an engine could be good if you are building something that is so niche that it requires specific optimizations.

ECMAScript was created as a standard to avoid chaos in the use of JavaScript. ECMA stands for European Computer Manufacturers Association. ECMA created ECMAScript as a standard for interoperability between web browsers.

Flash Cards:  

- Q:  
  - A:  

- Q:  
  - A:  

- Q:  
  - A:  

- Q:  
  - A:  

- Q:  
  - A:  

- Q:  
  - A:  

- Q:  
  - A:  

Sources:  


